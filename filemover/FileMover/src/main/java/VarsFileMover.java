
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class VarsFileMover {
    private String startDir = null;
    private String moveDir = null;
    private String fileParse = null;

    private void setStartDir(String startDir) {
        this.startDir = startDir;
    }

    public String getStartDir() {
        return startDir;
    }

    private void setMoveDir(String moveDir) {
        this.moveDir = moveDir;
    }

    public String getMoveDir() {
        return moveDir;
    }

    private void setFileParse(String fileParse) {
        this.fileParse = fileParse;
    }

    public String getFileParse() {
        return fileParse;
    }

    public void getVarsConfig(){
        File fileConfig = new File(new File("").getAbsoluteFile() + "/FileMover.properties");
        Properties prop = new Properties();
        FileInputStream fis;

        try {
            fis = new FileInputStream(fileConfig);
            prop.load(fis);
            setStartDir(prop.getProperty("dir.start"));
            setMoveDir(prop.getProperty("dir.tomove"));
            setFileParse(prop.getProperty("file.parse"));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}