import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class FileParse {

    private int count = 0;
    private int globalCount = 0;
    private ToolsFileMover tools = null;
    private String specialSymbol = null;
    public ArrayList<ArrayList<String>> writeListToMove = new ArrayList<ArrayList<String>>();
    public ArrayList<ArrayList<String>> writeListNotMove = new ArrayList<ArrayList<String>>();

    public FileParse(ToolsFileMover tools){

        this.tools = tools;
        this.specialSymbol = specialSymbol(tools.getDirFrom());
    }

    private String specialSymbol(String dir){
        if (dir.contains("/")){
            return "/";
        }
        return "\\";
    }

    private void fileReplace(String dirFrom, String dirTo, String fileFrom){

        count++;

        File fileFr = new File(dirFrom + fileFrom);
        File fileTo = new File(dirTo);

        if (!fileTo.exists()){
            fileTo.mkdirs();
        }

        fileFr.renameTo(new File(fileTo, fileFrom));
        if (count == 10000){
            globalCount += count;
            count = 0;
            System.out.println(globalCount);
        }
    }

    private void fileMoveDeleteOld(String dirFrom, String dirTo, String fileName){

        File file = new File(dirTo + fileName);
        if (file.exists()){
            file.delete();
        }

        fileReplace(dirFrom, dirTo, fileName);
    }

    private boolean checkExists(String fileName){

        File fileFrom = new File(tools.getDirFrom() + specialSymbol + fileName);
        if (fileFrom.exists()){
            return true;
        }
        return false;
    }

    public void parseFile(){

        try {
            XSSFWorkbook workbook = new XSSFWorkbook (tools.getFileParse());
            XSSFSheet sheet = workbook.getSheetAt(0);
            Row row = null;
            int maxRow = sheet.getLastRowNum();
            for (int i = 0; i < maxRow+1; i++){
                row = sheet.getRow(i);
                int temp1 = (int)row.getCell(0).getNumericCellValue();
                int temp2 = (int)row.getCell(2).getNumericCellValue();
                String dirTo = tools.getDirTo() + Integer.toString(temp1) + specialSymbol;
                fileReplace(Integer.toString(temp1), dirTo, Integer.toString(temp2));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void checkFileRemove(){

        int countFrom = 0;
        int countTo = 0;
        try {
            XSSFWorkbook workbook = new XSSFWorkbook (tools.getFileParse());
            XSSFSheet sheet = workbook.getSheetAt(0);
            Row row = null;
            int maxRow = sheet.getLastRowNum();
            System.out.println("maxRow = " + maxRow);
            for (int i = 0; i < maxRow+1; i++){
                row = sheet.getRow(i);
                String temp = row.getCell(2).getStringCellValue();
                if (checkExists(temp)){
                    writeListToMove.add(new ArrayList<String>());
                    writeListToMove.get(countTo).add(row.getCell(0).getStringCellValue());
                    writeListToMove.get(countTo).add(row.getCell(1).getStringCellValue());
                    writeListToMove.get(countTo).add(row.getCell(2).getStringCellValue());
                    writeListToMove.get(countTo).add(row.getCell(3).getStringCellValue());
                    countTo++;
                } else {
                    writeListNotMove.add(new ArrayList<String>());
                    writeListNotMove.get(countFrom).add(row.getCell(0).getStringCellValue());
                    writeListNotMove.get(countFrom).add(row.getCell(1).getStringCellValue());
                    writeListNotMove.get(countFrom).add(row.getCell(2).getStringCellValue());
                    writeListNotMove.get(countFrom).add(row.getCell(3).getStringCellValue());
                    countFrom++;
                }
                if ((i % 1000) == 0){
                    System.out.println(i + " is done");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void changeFileRemove(){

        checkFileRemove();
        int size = writeListToMove.size();
        for (int i = 0; i < size; i ++){
            String dirFrom = tools.getDirFrom();
            String dirTo = tools.getDirTo() + writeListToMove.get(i).get(0) + specialSymbol;
            String fileToMove = writeListToMove.get(i).get(2);
            fileMoveDeleteOld(dirFrom, dirTo, fileToMove);
        }
    }

    public void writeList() throws IOException {

        ArrayList<String> titleList = new ArrayList<String>();
        WriteInDoc wid = null;


        File writeFile = new File("FileToMove.xlsx");

        wid = new WriteFile(writeFile.getPath());
        titleList.add("ID компании");
        titleList.add("Название компании");
        titleList.add("ID файла");
        titleList.add("Название файла");

        wid.writeCheckFile(titleList, true, "Файлы на перемещение");

        int writeSize = writeListToMove.size();
        for (int i = 0; i < writeSize; i++){
            try {
                wid.writeCheckFile(writeListToMove.get(i), false, "");
            } catch (FileNotFoundException e) {
                System.out.println("Файл не найден");
                throw e;
            }
        }
            wid.close();

        writeFile = new File("FileNoMove.xlsx");

        wid = new WriteFile(writeFile.getPath());

        wid.writeCheckFile(titleList, true, "Файлы на перемещение");

        writeSize = writeListNotMove.size();
        for (int i = 0; i < writeSize; i++){
            try {
                wid.writeCheckFile(writeListNotMove.get(i), false, "");
            } catch (FileNotFoundException e) {
                System.out.println("Файл не найден");
                throw e;
            }
        }
        wid.close();
    }
}
