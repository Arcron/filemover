
public class ToolsFileMover {

    private String dirFrom = null;
    private String dirTo = null;
    private String fileParse = null;
    private int commandNumber = 0;

    public ToolsFileMover(String dirFrom, String dirTo, String fileParse, String command){

        setDirFrom(makeValid(dirFrom));
        setDirTo(this.dirTo = makeValid(dirTo));
        this.fileParse = fileParse;
        try {
            this.commandNumber = Integer.parseInt(command);
        } catch (NumberFormatException e){
            System.out.println("Введите число в поле command");
            throw e;
        }

        System.out.println("Просматриваемая директория = " + this.dirFrom);
        System.out.println("Директория для перемещения = " + this.dirTo);
        System.out.println("Файл список заглушек = " + this.fileParse);
        System.out.println("Номер команды = " + this.commandNumber);
    }

    private String makeValid(String dir){

        dir = dir.replaceAll("'","");
        if (dir.substring(0,1).equals("\\")){
            dir = dir.substring(1);
        }
        return dir;
    }

    private void setDirFrom(String dirFrom) {
        this.dirFrom = dirFrom;
    }

    public String getDirFrom() {
        return dirFrom;
    }

    private void setDirTo(String dirTo) {
        this.dirTo = dirTo;
    }

    public String getDirTo() {
        return dirTo;
    }

    private void setFileParse(String fileParse) {
        this.fileParse = fileParse;
    }

    public String getFileParse() {
        return fileParse;
    }

    private void setCommandNumber(int commandNumber) {
        this.commandNumber = commandNumber;
    }

    public int getCommandNumber() {
        return commandNumber;
    }
}
