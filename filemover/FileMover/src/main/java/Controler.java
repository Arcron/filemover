import java.io.IOException;

public class Controler {


    private ToolsFileMover tools = null;

    public Controler(ToolsFileMover tools){

        this.tools = tools;
    }

    public void run() throws IOException {


        FileParse fp = new FileParse(tools);
        switch (tools.getCommandNumber()){
            case 1:{
                fp.parseFile();
                break;
            }
            case 2:{
                fp.checkFileRemove();
                fp.writeList();
                break;
            }
            case 3:{
                fp.changeFileRemove();
                fp.writeList();
                break;
            }
            default:{
                System.out.println("Нет указанной команды");
                break;
            }
        }
    }
}
